#!/bin/bash -l
set -xeu
## NOTE: this file is public visible from web! Dont put passwords in here!
## WARN: This script runs as root!


# Base Packages
# -------------
<<EOF R -q --vanilla &> /tmp/rlog
need_pkg <- c('dplyr', 'tidyr', 'shiny', 'rmarkdown', 'shinyvalidate')
missing_pkg <- need_pkg[!(need_pkg %in% installed.packages()[,"Package"])]
if(length(missing_pkg)) install.packages(missing_pkg, repos='http://cran.utstat.utoronto.ca/')
EOF
cat /tmp/rlog
grep -q 'non-zero exit status' /tmp/rlog && exit 9 || true


# SAS Requirements
# -------------------------------------
apt install -y libpq-dev
<<EOF R -q --vanilla &> /tmp/rlog
need_pkg <- c('visNetwork', 'leaflet')
missing_pkg <- need_pkg[!(need_pkg %in% installed.packages()[,"Package"])]
if(length(missing_pkg)) install.packages(missing_pkg, repos='http://cran.utstat.utoronto.ca/')
devtools::install_github('https://github.com/jrowen/rhandsontable', ref = 'devel')
EOF
cat /tmp/rlog
grep -q 'non-zero exit status' /tmp/rlog && exit 9 || true


# clinton Requirements
# -------------------------------------
apt install -y libpq-dev
<<EOF R -q --vanilla &> /tmp/rlog
need_pkg <- c('stringr', 'SnowballC', 'chron', 'shinyBS', 'tm', 'grid')
missing_pkg <- need_pkg[!(need_pkg %in% installed.packages()[,"Package"])]
if(length(missing_pkg)) install.packages(missing_pkg, repos='http://cran.utstat.utoronto.ca/')
devtools::install_github('https://github.com/jrowen/rhandsontable', ref = 'devel')
EOF
cat /tmp/rlog
grep -q 'non-zero exit status' /tmp/rlog && exit 9 || true
