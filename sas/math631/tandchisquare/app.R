
### Data summaries, including ecdf/QQplot switching option

library(shiny)
library(rhandsontable)

shinyApp(
  ui <- fluidPage(
    titlePanel("Exploring Distributions"),
    sidebarLayout(
      sidebarPanel("Explore the t, and chi-square distributions, and their relationship with the Gaussian distribution!",

        wellPanel(

          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
                       c("t(k)" = "t",
                         "\u03C7\u00B2(k)" = "chisq"), inline = F),

          # radio buttons for input choice
          radioButtons("kinput", "Input method for k:",
                       c("Numeric" = "num",
                         "Slider" = "sli"), inline = F),

          ## parameter values
          # t
          conditionalPanel(
            condition = "input.dist == 't' & input.kinput == 'num'",
            numericInput(inputId = "tKnum",
                         label = "Enter k",
                         value = 3, min = 1, max = 100, step = 1),
          ),

          conditionalPanel(
            condition = "input.dist == 't' & input.kinput == 'sli'",
            sliderInput(inputId = "tKsli",
                         label = "Enter k",
                         value = 3, min = 1, max = 100, step = 1),
          ),

          # chi-suared
          conditionalPanel(
            condition = "input.dist == 'chisq' & input.kinput == 'num'",
            numericInput(inputId = "chiKnum",
                         label = "Enter k",
                         value = 1, min = 2, max = 200, step = 1),
          ),

          conditionalPanel(
            condition = "input.dist == 'chisq' & input.kinput == 'sli'",
            sliderInput(inputId = "chiKsli",
                         label = "Enter k",
                         value = 3, min = 2, max = 100, step = 1),
          ),


        # end of wellPanel
        ),
      # end of sidebarPanel
      ),
      # create main panel for outputs
      mainPanel(
        # output plot
                conditionalPanel(
                  condition = "input.tKnum > 0 & input.chiKnum % 1 == 0 & input.chiKnum > 0",
        plotOutput("distPlot"),
),

          # error message if invalid k
                conditionalPanel(
                  condition = "input.tKnum <= 0 | input.chiKnum % 1 != 0 | input.chiKnum <= 0",
img(src='pikachu.png', align = "center", width = "500px"),
                  textOutput("errorText"),
                  tags$head(tags$style("#errorText{font-size: 20px;
                                                 color: red;
                                                 font-style: bold;
                                                }"
                                      )
                           )          
                ),


      # end of mainPanel
      )
    # end of sidebarLayout
    )
  # end of ui
  ),

  server <- function(input, output) {


    output$distPlot <- renderPlot({
           # t distribution
          if (input$dist == 't') {
          if (input$kinput == 'num') {tk <- input$tKnum}
          if (input$kinput == 'sli') {tk <- input$tKsli}
           curve(dnorm(x, 0, 1),
                 col = "red", lty = 1, lwd = 3, xlim = c(-3, 3), xlab = NA, ylab = NA)
           curve(dt(x, tk),
                 col = "navy", add = TRUE, lty = 3, lwd = 3)
           legend('topright', legend = c('G(0,1)', paste('t(',tk,')', sep = '')), lty = c(1, 3), lwd = 3, col = c('red', 'navy'), cex = 2)
         }
          # chi-squared distribution
          if (input$dist == 'chisq') {
          if (input$kinput == 'num') {chik <- input$chiKnum}
          if (input$kinput == 'sli') {chik <- input$chiKsli}
           curve(dchisq(x, chik),
                 col = "navy", lty = 3, lwd = 3, xlim = c(0, chik*2),  xlab = NA, ylab = NA)
           curve(dnorm(x, chik, sqrt(2*chik)),
                 col = "red", add = TRUE, lty = 1, lwd = 3)
           legend('topright', legend = c(as.expression(bquote(paste('G(',.(chik), ',',sqrt(.(2*chik)),')', sep = ''))), bquote(paste(chi^2,'(',.(chik),')', sep = ''))), lty = c(1, 3), lwd = 3, col = c('red', 'navy'), cex = 2)
         }
    })

    output$errorText <- renderText({
         "Oh no! What's gone wrong?!"
    })


  #end or server
  }
# end of shinyApp
)



