


### Confidence Intervals

library(shiny)

shinyApp(
  ui <- fluidPage(
    titlePanel("Confidence Intervals"),
    sidebarLayout(
      sidebarPanel("Explore confidence intervals by generating multiple datasets: see how many cover the true parameter value!",

        wellPanel(

          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
                        c("Exponential(\u03B8)" = "exp",
                         "Binomial(n, \u03B8)" = "binom",
                         "Poisson(\u03B8)" = "pois"), inline = F),

          # numeric input for sample size:
          numericInput(inputId = "n",
                       label = "Enter sample size",
                       value = 100,
                       min = 10, max = 1000, step = 1),


          ## parameter values
          # exponential
          conditionalPanel(
            condition = "input.dist == 'exp'",
            numericInput(inputId = "expTheta",
                         label = "Enter \u03B8",
                         value = 1, min = 0.1, max = 5, step = 0.1),
          ),

          # Binomial
          conditionalPanel(
            condition = "input.dist == 'binom'",
            numericInput(inputId = "binomTheta",
                         label = "Enter \u03B8",
                         value = 0.5, min = 0.1, max = 0.9, step = 0.1),
          ),

          # Poisson
          conditionalPanel(
            condition = "input.dist == 'pois'",
            numericInput(inputId = "poisTheta",
                         label = "Enter \u03B8",
                         value = 5, min = 1, max = 30, step = 1),
          ),

          # numeric input for simulation runs:
          numericInput(inputId = "r",
                       label = "Enter number of samples",
                       value = 100,
                       min = 10, max = 1000, step = 1),


          # slider for confidence interval level
          sliderInput("confLevel", "Confidence level %:",
                      min = 0, max = 100, value = 95, step = 5),

          # resample button
          fluidRow(
            column(12, align = 'center',
            actionButton("resample", "Resample!"))
          )
        # end of wellPanel
        ),
      # end of sidebarPanel
      ),
      # create main panel for outputs
      mainPanel(

          # error message if n*r > 100000
                conditionalPanel(
                  condition = "input.n*input.r > 50000",
img(src='pikachu.png', align = "center", width = "500px"),
                  textOutput("nerrorText"),
                  tags$head(tags$style("#nerrorText{font-size: 20px;
                                                 color: red;
                                                 font-style: bold;
                                                }"
                                      )
                           )          
                ),

          # error message if 0% or 100% selected
                conditionalPanel(
                  condition = "input.confLevel == 0 | input.confLevel == 100",
img(src='pikachu.png', align = "center", width = "500px"),
                  textOutput("errorText"),
                  tags$head(tags$style("#errorText{font-size: 20px;
                                                 color: red;
                                                 font-style: bold;
                                                }"
                                      )
                           )          
                ),

          # otherwise show CIs
                conditionalPanel(
                  condition = "input.n*input.r <= 50000 & input.confLevel > 0 & input.confLevel < 100",

                  # output table of first 20 estimates
                  textOutput("cisText"),
                  tags$head(tags$style("#cisText{font-size: 20px;
                                                 font-style: bold;
                                                }"
                           )          ),


h3("First 20 CIs:"),

          splitLayout(cellWidths = c("50%", "50%"),
                  tableOutput("CIs1"),                  
tableOutput("CIs2")
),

                 plotOutput("ciPlot"),

#          splitLayout(cellWidths = c("50%", "50%"),
 #                 plotOutput("ciPlot"),                  
#tableOutput("CIs")
#)
                ),


      # end of mainPanel
      )
    # end of sidebarLayout
    )
  # end of ui
  ),

  server <- function(input, output) {

    # generate sample of observations from selected distribution
    datasetInput <- reactive({
if (input$n*input$r <= 50000) {
    # use action button to resample when clicked
    input$resample
    switch(input$dist,
           norm = matrix(rnorm(input$n * input$r, input$normMu, input$normSig), ncol = input$r),
           exp = matrix(rexp(input$n * input$r, 1/input$expTheta), ncol = input$r),
           binom = matrix(rbinom(input$n * input$r, 1, input$binomTheta), ncol = input$r),
           pois = matrix(rpois(input$n * input$r, input$poisTheta), ncol = input$r))
}
    })


    # CI functions
    CI.lower <- function(x, dist) {
      switch(dist,
         exp = mean(x) - qnorm((1 + input$confLevel/100)/2)*mean(x)/sqrt(input$n),
         binom = mean(x) - qnorm((1 + input$confLevel/100)/2)*sqrt(mean(x)*(1 - mean(x))/input$n),
         pois = mean(x) - qnorm((1 + input$confLevel/100)/2)*sqrt(mean(x)/input$n))
    }

    CI.upper <- function(x, dist) {
      switch(dist,
         exp = mean(x) + qnorm((1 + input$confLevel/100)/2)*mean(x)/sqrt(input$n),
         binom = mean(x) + qnorm((1 + input$confLevel/100)/2)*sqrt(mean(x)*(1 - mean(x))/input$n),
         pois = mean(x) + qnorm((1 + input$confLevel/100)/2)*sqrt(mean(x)/input$n))
    }


        ##### plot of CIs
        output$ciPlot <- renderPlot({
        # true parameter value for output
        trueParam <- switch(input$dist,
               norm = input$normMu,
               exp = input$expTheta,
               binom = input$binomTheta,
               pois = input$poisTheta)

            sampFull <- datasetInput()
            lower <- apply(sampFull, 2, CI.lower, dist = input$dist)
            upper <- apply(sampFull, 2, CI.upper, dist = input$dist)

       # evaluate coverage
            cover <- as.numeric(lower < trueParam & upper > trueParam)

            plot(NA, NA, xlim = c(0, input$r), ylim = c(min(lower, upper), max(lower, upper)), xaxt = "n", xlab = NA, ylab = "Parameter estimate", main = paste("Approximate ", input$confLevel, "% confidence intervals from ", input$r, " samples of size n = ", input$n, sep = ""))
            segments(c(1:input$r), lower, c(1:input$r), upper, lty = 3 - 2*cover, lwd = 2, col = 2 - cover)
            abline(h = trueParam, lwd = 3, col = 'navy', lty = 3)

   #         plot(NA, NA, ylim = c(0, input$r), xlim = c(min(lower, upper), max(lower, upper)), yaxt = "n", ylab = NA, xlab = "Parameter estimate", main = paste("Approximate ", input$confLevel, "% CIs from ", input$r, " samples of size n = ", input$n, sep = ""))
    #        segments(lower, c(1:input$r), upper, c(1:input$r), lty = 3 - 2*cover, lwd = 2, col = 2 - cover)
     #       abline(v = trueParam, lwd = 3, col = 'navy', lty = 3)


        })

    output$nerrorText <- renderText({
         "Sample size and/or number of samples too large! Try reducing them."
    })


    output$errorText <- renderText({
         "Oh no! Can't display confidence intervals! Why?"
    })
    # first 10 CIs
    output$CIs1 <- renderTable(digits = 3,{
	# true parameter
       trueParam <- switch(input$dist,
               norm = input$normMu,
               exp = input$expTheta,
               binom = input$binomTheta,
               pois = input$poisTheta)
	# data
            sampFull <- datasetInput()
            lower <- apply(sampFull, 2, CI.lower, dist = input$dist)
            upper <- apply(sampFull, 2, CI.upper, dist = input$dist)

       # evaluate coverage
            cover <- as.numeric(lower < trueParam & upper > trueParam)
            covercheck <- cover
            covercheck[cover == 0] <- "Outside :("
            covercheck[cover == 1] <- "Inside :)"
      # return CIs
      data.frame(Sample = c(1:10), Lower = lower[1:10], Upper = upper[1:10], Truth = covercheck[1:10])
    })

    output$CIs2 <- renderTable(digits = 3,{
	# true parameter
       trueParam <- switch(input$dist,
               norm = input$normMu,
               exp = input$expTheta,
               binom = input$binomTheta,
               pois = input$poisTheta)
	# data
            sampFull <- datasetInput()
            lower <- apply(sampFull, 2, CI.lower, dist = input$dist)
            upper <- apply(sampFull, 2, CI.upper, dist = input$dist)

       # evaluate coverage
            cover <- as.numeric(lower < trueParam & upper > trueParam)
            covercheck <- cover
            covercheck[cover == 0] <- "Outside :("
            covercheck[cover == 1] <- "Inside :)"
      # return CIs
      data.frame(Sample = c(11:20), Lower = lower[11:20], Upper = upper[11:20], Truth = covercheck[11:20])
    })

    output$cisText <- renderText({
        # parameter notation for output (mu or theta)
        param <- switch(input$dist,
                        norm = '\u03BC',
                        '\u03B8')
        # true parameter value for output
        trueParam <- switch(input$dist,
               exp = input$expTheta,
               binom = input$binomTheta,
               pois = input$poisTheta)

            sampFull <- datasetInput()
            lower <- apply(sampFull, 2, CI.lower, dist = input$dist)
            upper <- apply(sampFull, 2, CI.upper, dist = input$dist)

       # evaluate coverage
            cover <- as.numeric(lower < trueParam & upper > trueParam)



        paste(input$r, " samples of size n = ", input$n, " were generated, producing ", input$r, " approximate ", input$confLevel, "% confidence intervals. Of these, ", sum(cover), " (", round(100*sum(cover)/input$r, 1), "%) contain the true parameter value ", param, " = ", trueParam, ".", sep = "")
    })

  #end or server
  }
# end of shinyApp
)



