
### Data summaries, including ecdf/QQplot switching option

library(shiny)
library(rhandsontable)
#library(shinyvalidate)


shinyApp(
  ui <- fluidPage(
    titlePanel("Data summaries"),
    sidebarLayout(
      sidebarPanel("Randomly generate datasets: new samples can be generated automatically by changing distribution, sample size, model parameters, or clicking the 'Resample!' button.",

        wellPanel(

#    dropdown list for distribution (use if distribution list gets too long)
#    selectInput("dist", "Select distribution:",
#              c("G(\u03BC,\u03C3)" = "norm",
#                         "Uniform(a, b)" = "unif",
#                         "Exponential(\u03B8)" = "exp",
#                         "Negative Exponential(\u03B8)" = "negexp",
#                         "t(k)" = "t")),

          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
                       c("G(\u03BC,\u03C3)" = "norm",
                         "Uniform(a, b)" = "unif",
                         "Exponential(\u03B8)" = "exp",
                         "Negative Exponential(\u03B8)" = "negexp",
                         "t(k)" = "t"), inline = F),

          # numeric input for sample size:
          numericInput(inputId = "n",
                       label = "Enter sample size",
                       value = 100,
                       min = 10, max = 1000, step = 1),

          ## parameter values
          # normal
          conditionalPanel(
            condition = "input.dist == 'norm'",
            numericInput(inputId = "normMu",
                         label = "Enter \u03BC",
                         value = 0, min = -5, max = 5, step = 0.1),
            numericInput(inputId = "normSig",
                         label = "Enter \u03C3",
                         value = 1, min = 0, max = 5, step = 0.1),
          ),

          # uniform
          conditionalPanel(
            condition = "input.dist == 'unif'",
            numericInput(inputId = "unifA",
                         label = "Enter a",
                         value = 0, min = -5, max = 5, step = 0.1),
            numericInput(inputId = "unifB",
                         label = "Enter b",
                         value = 1, min = -5, max = 5, step = 0.1),
          ),

          # exponential and negative exponential
          conditionalPanel(
            condition = "input.dist == 'exp' | input.dist == 'negexp'",
            numericInput(inputId = "expTheta",
                         label = "Enter \u03B8",
                         value = 1, min = 0.1, max = 5, step = 0.1),
          ),

          # t
          conditionalPanel(
            condition = "input.dist == 't'",
            numericInput(inputId = "tK",
                         label = "Enter k",
                         value = 3, min = 2, max = 100, step = 1),
          ),

          # slider for number of bins in histogram
          sliderInput("bins", "Select number of bins:",
                      min = 1, max = 100, value = 10),

          # radio button to choose second plot
          radioButtons("plotchoice", "Choose second plot:",
                       c("ecdf" = "ecdf",
                         "QQ plot" = "qq"), inline = T),

          # resample button
          fluidRow(
            column(12, align = 'center',
            actionButton("resample", "Resample!"))
          )
        # end of wellPanel
        ),
      # end of sidebarPanel
      ),
      # create main panel for outputs
      mainPanel(

          # error message if n*r > 100000
                conditionalPanel(
                  condition = "input.n > 1000",
img(src='pikachu.png', align = "center", width = "500px"),
                  textOutput("nerrorText"),
                  tags$head(tags$style("#nerrorText{font-size: 20px;
                                                 color: red;
                                                 font-style: bold;
                                                }"
                                      )
                           )          
                ),


        # output plots
        fluidRow(

          splitLayout(cellWidths = c("50%", "50%"),
                      conditionalPanel(condition = "input.n <= 10000",plotOutput("histPlot")),
                      conditionalPanel(condition = "input.n <= 10000",plotOutput("secondPlot")))
        ),






        # output summary statistics
        conditionalPanel(condition = "input.n <= 1000",tableOutput("dataSummary"))
#),
      # end of mainPanel
      )
    # end of sidebarLayout
    )
  # end of ui
  ),

  server <- function(input, output) {

#iv <- InputValidator$new()
#  iv$add_rule("n", sv_between(0, 10000))
#  iv$enable()

    # generate sample of observations from selected distribution
    datasetInput <- reactive({
    # use action button to resample when clicked
    input$resample
if (input$n <= 1000) {
    switch(input$dist,
           norm = rnorm(input$n, input$normMu, input$normSig),
           unif = runif(input$n, input$unifA, input$unifB),
           exp = rexp(input$n, 1/input$expTheta),
           negexp = -rexp(input$n, 1/input$expTheta),
           t = rt(input$n, input$tK))
}
    })
    output$histPlot <- renderPlot({
      # retrieve sample of observations
      s <- datasetInput()
      # plot histogram
      hist(s,
           breaks = input$bins,
           col = "dodgerblue3",
           density = 27, angle = 43,
           xlab = "Sample value",
           main = "Histogram of Sample",
           prob = T,
           ylim = c(0, max(hist(s, breaks = input$bins)$density, dnorm(mean(s), mean(s), sd(s)))))
           # and normal ditsribution curve based on x
           curve(dnorm(x, mean(s),sd(s)),
                 col = "red", add = TRUE, lty = 1, lwd = 2)
    })

    ## second plot: either ecdf or QQ dependent on input
    output$secondPlot <- renderPlot({
      # retrieve sample of observations
      s <- datasetInput()

      # default is to show ecdf
      plot(ecdf(s), verticals = T, do.points = F,
           xlab = "", ylab = "ecdf",
           main = "Empirical and Gaussian CDFs", lwd = 2, col = 'navy')
      # superimpose Gaussian cdf
      curve(pnorm(x, mean(s), sd(s)),
            add = TRUE, col = "red", lwd=2, lty = 2) 

      if (input$plotchoice == "qq") {
        # qq plot
        qqnorm(s,
               xlab = 'G(0,1) Theoretical Quantiles',
               main = "QQ plot of Sample",
               col = 'navy')
               qqline(s, col = "red", lwd = 2)
      }
    })

    # skewness function
    skewness <- function(x) {
      n <- length(x)
      xbar <- mean(x)
      numerator <- (1/n)*sum((x - xbar)^3)
      denominator <- ((1/n)*sum((x - xbar)^2))^(3/2)
      numerator/denominator
    }

    # kurtosis function
    kurtosis <- function(x) {
      n <- length(x)
      xbar <- mean(x)
      numerator <- (1/n)*sum((x - xbar)^4)
      denominator <- ((1/n)*sum((x - xbar)^2))^(2)
      numerator/denominator
    }


    output$dataSummary <- renderTable(digits = 3,{
      # retrieve sample of observations
      s <- datasetInput()
      # return summary statistics
      data.frame(Statistic = c('Mean', 'Median'), Value = c(mean(s), median(s)), Statistic = c('Variance', 'IQR'), Value = c(var(s), quantile(s, 0.75) - quantile(s, 0.25)), Statistic = c('Skewness', 'Kurtosis'), Value = c(skewness(s), kurtosis(s)), check.names = F)
    })
  #end or server
  }
# end of shinyApp
)



