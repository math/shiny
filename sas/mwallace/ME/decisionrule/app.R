
# STAT 231 Assignment 1: Do it for the (insta)gram

library(shiny)

shinyApp(

  ui <- fluidPage(
    titlePanel("Mistreatment Probabilities"),
    sidebarLayout(
      sidebarPanel("This app explores the probability that an incorrect treatment decision is made as a result of measurement error in a tailoring covariate. Error-prone data of the form X* = X + U are generated, with X and U normally distributed. A treatment rule of the form 'Treat if X > t' is applied, for some treatment threshold t. The resulting graph shows the probability that an incorrect treatment decision is made if it is based on the error-prone X*. For example, if X* = 16, X = 14, and the treatment rule is 'treat if X > 15'.",

        wellPanel(

          # mean of X
          numericInput(inputId = "mux",
                       label = "Enter mean of X",
                       value = 15,
                       min = -100, max = 100, step = 1),
          numericInput(inputId = "varx",
                       label = "Enter variance of X",
                       value = 2,
                       min = 1, max = 100, step = 1),
          numericInput(inputId = "varu",
                       label = "Enter variance of U",
                       value = 2,
                       min = 1, max = 100, step = 1),
          numericInput(inputId = "tau",
                       label = "Enter treatment threshold",
                       value = 15,
                       min = 1, max = 100, step = 1),
          sliderInput(inputId = "zoom",
                       label = "Adjust plot range",
                       value = 4,
                       min = 1, max = 10, step = 1),

          # slider for number of bins in histogram
#          sliderInput("bins", "Select number of bins:",
 #                     min = 1, max = 100, value = 10),

#        fluidRow(
 #         column(12, align = 'center',
  #        actionButton("resample", "Resample!"))
   #     )

      # end of wellPanel
      ),
    # end of sidebarPanel
    ),
  # create main panel for outputs
  mainPanel(
    # output plots
    fluidRow(


     plotOutput("qqPlot", height = 700, width = 700))

    # output summary statistics
 #   conditionalPanel(condition = "input.n <= 10000",tableOutput("dataSummary"))

  # end of mainPanel
  )
# end of sidebarLayout
)
# end of ui
),

server <- function(input, output) {

  # generate sample of observations from selected distribution
  datasetInput <- reactive({
  # use action button to resample when clicked


set.seed(input$userid)
# choose models
model.list <- c(1, sample(2:4, 1), sample(5:8, 1))
# choose order
model.order <- sample(model.list, 3)
model <- model.order[as.numeric(input$dist)]



#  input$resample
set.seed(Sys.time())
if (input$n <= 10000) {

# model 1 = Gaussian
# models 2-4 = Uniform, Logistic, t(k)
# models 5-8 = Exponential, Negative Exponential, Log-Normal, Negative Log-Normal
  switch(model,
         rnorm(input$n),
         runif(input$n),
         rlogis(input$n)^3,
         rt(input$n, 3),
         rexp(input$n),
         -rexp(input$n),
         rlnorm(input$n),
         -rlnorm(input$n))
}
  })


  ## qq plot
  output$qqPlot <- renderPlot({

mu.x <- input$mux
sig2.u <- input$varu
sig2.x <- input$varx
tau <- input$tau

w.range <- seq(tau - sig2.x*(input$zoom/2)^2, tau + sig2.x*(input$zoom/2)^2, 0.01)

prob <- c()
for (w in w.range) {
	# this flips once |w - tau| switches sign
	xw.mean <- (sig2.u * mu.x + sig2.x * w)/(sig2.x + sig2.u)
	xw.var <- (sig2.u * sig2.x)/(sig2.x + sig2.u)
	if (w < tau) {
		prob <- append(prob, 1 - pnorm(tau, xw.mean, sqrt(xw.var)))
#		points(i, 1 - pnorm(tau, xw.mean, sqrt(xw.var)), pch = 16, col = 3)
	} else {
		prob <- append(prob, pnorm(tau, xw.mean, sqrt(xw.var)))
#		points(i, pnorm(tau, xw.mean, sqrt(xw.var)), pch = 16, col = 3)
	}
}



par(mar = c(5.1, 5.1, 3.1, 2.1))
plot(w.range, prob, type = 'l', lwd = 3, col = 'dodgerblue2', xlab = 'Observed X = True X + Error', ylab = NA, cex.lab = 1.5, cex.axis = 1.5, col.lab = 'black', col.axis = 'black', axes = F, main = paste('True X ~ N(', mu.x, ', ', sig2.x, ')   Treat if X > ', tau, sep = ''), col.main = 'black', cex.main = 1.5)
axis(1, seq(min(w.range), max(w.range), 1), col = 'black', col.lab = 'black', col.axis = 'black', cex.lab = 1.5, cex.axis = 1.5)
axis(2, col = 'black', col.lab = 'black', col.axis = 'black', cex.lab = 1.5, cex.axis = 1.5, las = 1)
abline(v = tau, lty = 2, lwd = 3, col = 'pink')

#par(bg = "black")
#par(mar = c(5.1, 5.1, 3.1, 2.1))
#plot(w.range, prob, type = 'l', lwd = 3, col = 'dodgerblue2', xlab = 'Observed X = True X + Error', ylab = NA, cex.lab = 2, cex.axis = 2, #col.lab = 'white', col.axis = 'white', axes = F, main = paste('True X ~ N(', mu.x, ', ', sig2.x, ')   Treat if X > ', tau, sep = ''), col.main = 'white', cex.main = 2)
#axis(1, seq(min(w.range), max(w.range), 1), col = 'white', col.lab = 'white', col.axis = 'white', cex.lab = 2, cex.axis = 2)
#axis(2, col = 'white', col.lab = 'white', col.axis = 'white', cex.lab = 2, cex.axis = 2, las = 1)
#abline(v = tau, lty = 2, lwd = 3, col = 'pink')


# end of renderplot
    })

 
  #end or server
  }
# end of shinyApp
)



