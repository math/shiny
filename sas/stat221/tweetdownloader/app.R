### Data summaries, including ecdf/QQplot switching option

## TODO:
## change first tweet to

library(shiny)
library(rhandsontable)

shinyApp(
  ui <- fluidPage(
  # App title ----
  titlePanel("STAT 221 Twitter Dataset Downloader"),

  # Sidebar layout with input and output definitions ----
  sidebarLayout(

    # Sidebar panel for inputs ----
    sidebarPanel(
"Enter the password (found on LEARN), your student ID number, and select the accounts you wish to sample tweets from. You must select five accounts: three personal accounts and two organizational accounts. Once you have entered the correct password, a valid ID number and selected five different accounts a 'Download' button will appear. Click the button to download your sample from the primary dataset. Once your sample is downloaded, please upload it to the LEARN dropbox *immediately*! :)",

h2("", style = "font-size:25px;"),

textInput("password", "Enter password:", value = "", width = NULL, placeholder = NULL),

      # Input: Choose dataset ----
      numericInput("userid", "Enter your ID number:", value = 00000000, min = 20000000, max = 29999999),

      # Input: Choose accounts
    selectInput("account1", "Personal account 1:",
                c("@alessiacara", "@Alethea_Aggiuq", "@amanda_parris", "@b0rk", "@BardishKW", "@CPHO_Canada", "@kareem_carr", "@harleyf", "@JohnTory", "@jonnysun", "@kevinolearytv", "@LauraMaeLindo", "@MargaretAtwood", "@morricemike", "@MrAhmednurAli", "@ScottAukerman", "@SimuLiu", "@tagaq", "@thehazelmae", "@theJagmeetSingh")),
    selectInput("account2", "Personal account 2:",
                c("@alessiacara", "@Alethea_Aggiuq", "@amanda_parris", "@b0rk", "@BardishKW", "@CPHO_Canada", "@kareem_carr", "@harleyf", "@JohnTory", "@jonnysun", "@kevinolearytv", "@LauraMaeLindo", "@MargaretAtwood", "@morricemike", "@MrAhmednurAli", "@ScottAukerman", "@SimuLiu", "@tagaq", "@thehazelmae", "@theJagmeetSingh")),
    selectInput("account3", "Personal account 3:",
                c("@alessiacara", "@Alethea_Aggiuq", "@amanda_parris", "@b0rk", "@BardishKW", "@CPHO_Canada", "@kareem_carr", "@harleyf", "@JohnTory", "@jonnysun", "@kevinolearytv", "@LauraMaeLindo", "@MargaretAtwood", "@morricemike", "@MrAhmednurAli", "@ScottAukerman", "@SimuLiu", "@tagaq", "@thehazelmae", "@theJagmeetSingh")),
    selectInput("account4", "Organizational account 1:",
                c("@AnishNation", "@CTVKitchener", "@FANEXPOCANADA", "@ONThealth", "@OntScienceCtr", "@PrideToronto", "@RaptorsGC", "@ROMtoronto", "@UWaterloo", "@TheTorontoZoo")),
    selectInput("account5", "Organizational account 2:",
                c("@AnishNation", "@CTVKitchener", "@FANEXPOCANADA", "@ONThealth", "@OntScienceCtr", "@PrideToronto", "@RaptorsGC", "@ROMtoronto", "@UWaterloo", "@TheTorontoZoo")),
      # Button


          conditionalPanel(
            condition = "input.account1 != input.account2 & input.account1 != input.account3 & input.account1 != input.account4 & input.account1 != input.account5 & input.account2 != input.account3 & input.account2 != input.account4 & input.account2 != input.account5 & input.account3 != input.account4 & input.account3 != input.account5 & input.account4 != input.account5 & input.userid > 19999999 & input.userid < 30000000 & input.password == 'datumsplz'",
      downloadButton("downloadData", "Download"),
     align = "center"
          ),

    ),

    # Main panel for displaying outputs ----
    mainPanel(
#      tableOutput("orgtable")

        # output org tables
        fluidRow(
          h2(("Tables give number of followers of each account at time of primary dataset download, and number of tweets (n) and date of first tweet (Start) per account in primary dataset."), style = "font-size:25px;"),

         h2(strong("Personal"), style = "font-size:18px;"),
          splitLayout(cellWidths = c("40%", "40%"),
                            tableOutput("personaltable1"),
                            tableOutput("personaltable2")),
         h2(strong("Organizational"), style = "font-size:18px;"),
          splitLayout(cellWidths = c("40%", "40%"),
                            tableOutput("orgtable1"),
                            tableOutput("orgtable2"))

     # end of fluidrow
        ),
   # end of mainpanel
    )
  # end of sidebarlayout
  )
  # end of ui
  ),

  server <- function(input, output) {
   # Reactive value for selected dataset ----
  datasetInput <- reactive({
     dataset <- read.csv('TweetsProcessedF22.csv')
    # sample sizes
    set.seed(input$userid)
     sampsize <- sample(200:250, 5)
    # sample first account
     acct1 <- subset(dataset, subset = (username == input$account1))
     acct1 <- acct1[sample(nrow(acct1), sampsize[1]), ]
    # sample second account
     acct2 <- subset(dataset, subset = (username == input$account2))
     acct2 <- acct2[sample(nrow(acct2), sampsize[2]), ]
    # sample third account
     acct3 <- subset(dataset, subset = (username == input$account3))
     acct3 <- acct3[sample(nrow(acct3), sampsize[3]), ]
    # sample fourt account
     acct4 <- subset(dataset, subset = (username == input$account4))
     acct4 <- acct4[sample(nrow(acct4), sampsize[4]), ]
    # sample fift account
     acct5 <- subset(dataset, subset = (username == input$account5))
     acct5 <- acct5[sample(nrow(acct5), sampsize[5]), ]
    # combine samples and return
     dataset <- rbind(acct1, acct2, acct3, acct4, acct5)
     dataset
  })

  # Downloadable csv of selected dataset ----
  output$downloadData <- downloadHandler(
    filename = function() {
      paste("dataset",input$userid, ".csv", sep = "")
    },
    content = function(file) {
      write.csv(datasetInput(), file, row.names = FALSE)
    }
  )

  # Table of summary information ----
  output$orgtable1 <- renderTable({
    Tweets <- read.csv('TweetsProcessedF22.csv')
    # Username, Tweets, First Tweet
    # aim for two tables, people and orgs
#    accounts <- unique(Tweets$username)
    # separate into personal/org
#    act <- accounts[c(3, 11, 18, 19, 20, 21, 22, 28, 29, 30)]
#    act <- act[1:5]
     act <- c("@AnishNation", "@CTVKitchener", "@FANEXPOCANADA", "@ONThealth", "@OntScienceCtr")

   # create org table
   act.n <- act.first <- act.followers <- c()
   for (acc in act) {
      act.n <- append(act.n, sum(Tweets$username == acc))
      act.first <- append(act.first, min(as.Date(Tweets$published[Tweets$username == acc])))
      act.followers <- append(act.followers, Tweets$followers[Tweets$username == acc][1])
   }
   data.frame(Username = act, Followers = act.followers, n = act.n, Start = as.character(act.first))
  })

  output$orgtable2 <- renderTable({
    Tweets <- read.csv('TweetsProcessedF22.csv')
    # get accounts
     act <- c("@PrideToronto", "@RaptorsGC", "@ROMtoronto", "@UWaterloo", "@TheTorontoZoo")

   # create org table
   act.n <- act.first <- act.followers <- c()
   for (acc in act) {
      act.n <- append(act.n, sum(Tweets$username == acc))
      act.first <- append(act.first, min(as.Date(Tweets$published[Tweets$username == acc])))
      act.followers <- append(act.followers, Tweets$followers[Tweets$username == acc][1])
   }
   data.frame(Username = act, Followers = act.followers, n = act.n, Start = as.character(act.first))
  })

  output$personaltable1 <- renderTable({
    Tweets <- read.csv('TweetsProcessedF22.csv')
    # Username, Tweets, First Tweet
    act <- c("@alessiacara", "@Alethea_Aggiuq", "@amanda_parris", "@b0rk", "@BardishKW", "@CPHO_Canada", "@harleyf", "@JohnTory", "@jonnysun", "@kareem_carr")

   # create org table
   act.n <- act.first <- act.followers <- c()
   for (acc in act) {
      act.n <- append(act.n, sum(Tweets$username == acc))
      act.first <- append(act.first, min(as.Date(Tweets$published[Tweets$username == acc])))
      act.followers <- append(act.followers, Tweets$followers[Tweets$username == acc][1])
   }
   data.frame(Username = act, Followers = act.followers, n = act.n, Start = as.character(act.first))
  })
  output$personaltable2 <- renderTable({
    Tweets <- read.csv('TweetsProcessedF22.csv')
    # Username, Tweets, First Tweet
    # aim for two tables, people and orgs
    act <- c("@kevinolearytv", "@LauraMaeLindo", "@MargaretAtwood", "@morricemike", "@MrAhmednurAli", "@ScottAukerman", "@SimuLiu", "@tagaq", "@thehazelmae", "@theJagmeetSingh")

   # create org table
   act.n <- act.first <- act.followers <- c()
   for (acc in act) {
      act.n <- append(act.n, sum(Tweets$username == acc))
      act.first <- append(act.first, min(as.Date(Tweets$published[Tweets$username == acc])))
      act.followers <- append(act.followers, Tweets$followers[Tweets$username == acc][1])
   }
   data.frame(Username = act, Followers = act.followers, n = act.n, Start = as.character(act.first))
  })

  #end or server
  }
# end of shinyApp
)



