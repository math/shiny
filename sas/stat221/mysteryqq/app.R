
# STAT 231 Assignment 1: Do it for the (insta)gram

library(shiny)

shinyApp(

  ui <- fluidPage(
    titlePanel("STAT 231: Mystery QQ Plots"),
    sidebarLayout(
      sidebarPanel("Enter your student ID number to activate the app. New samples can be generated automatically by changing sample size or clicking the 'Resample!' button.",

        wellPanel(

      numericInput("userid", "Enter your ID number:", value = 00000000, min = 20000000, max = 29999999),

conditionalPanel(
 condition = "input.userid > 19999999 & input.userid < 30000000",
          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
                       c("A" = 1,
                         "B" = 2,
                         "C" = 3), inline = F),

          # numeric input for sample size:
          sliderInput(inputId = "n",
                       label = "Enter sample size",
                       value = 100,
                       min = 50, max = 1000, step = 50),

          # slider for number of bins in histogram
#          sliderInput("bins", "Select number of bins:",
 #                     min = 1, max = 100, value = 10),

        fluidRow(
          column(12, align = 'center',
          actionButton("resample", "Resample!"))
        )
# end of conditional
)
      # end of wellPanel
      ),
    # end of sidebarPanel
    ),
  # create main panel for outputs
  mainPanel(
          # error message if n*r > 100000
                conditionalPanel(
                  condition = "input.userid < 20000000 | input.userid > 29999999",
img(src='mystery.png', align = "center", width = "500px")      
                ),


    # output plots
    fluidRow(

#                  conditionalPanel(condition = "input.n <= 10000",plotOutput("histPlot")),
                  conditionalPanel(condition = "input.userid > 19999999 & input.userid < 30000000", plotOutput("qqPlot", height = 500, width = 500)))

    # output summary statistics
 #   conditionalPanel(condition = "input.n <= 10000",tableOutput("dataSummary"))

  # end of mainPanel
  )
# end of sidebarLayout
)
# end of ui
),

server <- function(input, output) {

  # generate sample of observations from selected distribution
  datasetInput <- reactive({
  # use action button to resample when clicked


set.seed(input$userid)
# choose models
model.list <- c(1, sample(2:4, 1), sample(5:8, 1))
# choose order
model.order <- sample(model.list, 3)
model <- model.order[as.numeric(input$dist)]



  input$resample
set.seed(Sys.time())
if (input$n <= 10000) {

# model 1 = Gaussian
# models 2-4 = Uniform, Logistic, t(k)
# models 5-8 = Exponential, Negative Exponential, Log-Normal, Negative Log-Normal
  switch(model,
         rnorm(input$n),
         runif(input$n),
         rlogis(input$n)^3,
         rt(input$n, 3),
         rexp(input$n),
         -rexp(input$n),
         rlnorm(input$n),
         -rlnorm(input$n))
}
  })


  ## qq plot
  output$qqPlot <- renderPlot({
    # retrieve sample of observations
    s <- datasetInput()
    # qq plot
    qqnorm(s,
           xlab = 'G(0,1) Theoretical Quantiles',
           main = paste("QQ plot of Sample for", input$userid),
           col = 'navy')
           qqline(s, col = "red", lwd = 2)
    })

 
  #end or server
  }
# end of shinyApp
)



